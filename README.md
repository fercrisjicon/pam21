# LDAP @edt ASIX M06-ASO
## Curs 2021-2022

* **pam21:base** Dades base de l'imatge PAM.
	Organitzacio: edt.org
* **pam21:pam_time.so** Maquina PAM practica de permissos de temps de chfn.
	Organitzacio: edt.org
* **pam21:mount** Maquina PAM practica de muntar sysfiles a usuaris.
	Organitzacio: edt.org
* **pam21:ldap** Maquina PAM practica per autiritzar l'acces a usuaris LDAP a un servidor LDAP.
	Organitzacio: edt.org
