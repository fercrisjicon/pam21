#! /bin/bash
# creem els usuaris UNIX, amb els seus directoris i shells
useradd -m -s /bin/bash unix01
useradd -m -s /bin/bash unix02
useradd -m -s /bin/bash unix03
# els possem un password a cada UNIX
echo -e "unix01\nunix01" | passwd unix01
echo -e "unix02\nunix02" | passwd unix02
echo -e "unix03\nunix03" | passwd unix03

cp /opt/docker/login.defs /etc/login.defs
cp /opt/docker/chfn /etc/pam.d/chfn

# instal·lar el modul pam_python
pip install python-pam
# arrencar
/bin/bash
