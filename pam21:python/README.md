# PAM Server
## @edt ASIX M06-ASO 2021-2022
### Servidor PAM (Debian 11)

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


 * **isx41016667/pam21:python** Servidor PAM base amb programa python amb el modul PAM per python.

```
entrar dind del servidor PAM
docker run --rm --name pam.edt.org -h pam.edt.org --net hisix2 -it cristiancondolo21/pam21:python

provem el priemr programa pamaware.py:
python3 pamaware.py
Nom d'usuari: marti
Passwd: marti
7 Authentication failure
Error d'Autenticacio

root@pam:/opt/docker# python3 pamaware.py
Nom d'usuari: unix02
Passwd: unix02
0 Success
1 2 3 4 5 6 7 8 9 10
``` 
