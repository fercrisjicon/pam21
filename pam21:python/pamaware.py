#!/usr/bin/python
#-*- coding: utf-8-*-
# Cristia Condolo Jimenez
# pamaware.py
#===============================
import pam

p=pam.pam()
userName=input("Nom d'usuari: ")
userPassword=input("Passwd: ")

p.authenticate(userName,userPassword)
print('{} {}'.format(p.code,p.reason))

if p.code == 0:
    for i in range(1,11):
        print(i)
else:
    print("Error d'Autenticacio")